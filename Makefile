all: yarn lint script html

build: script html

yarn:
	yarn install --production=false

script: yarn
	npx tsc src/js/*.ts || true # || true necessary due to odd errors emitted by tsc, resulting in non-zero exit code
	npx webpack-cli

html: yarn
	cp -rf src/html/. public/
	npx grunt

lint: yarn
	npx tslint src/js/*.ts --fix

test: all
	npx node --eval "var open = require('open'); open('public/index.html')"
